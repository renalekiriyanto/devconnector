const express = require('express')
const router = express.Router()
const auth = require('../../middleware/auth')
const User = require('../../models/User')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const {check, validationResult} = require('express-validator')
const config = require('config')

router.get('/', auth, async (req, res) => {
    try{
        const user = await User.findById(req.user.id).select('-password')
        res.json(user);
    }catch(err){
        console.error(err.message)
        res.status(500).send('Server error')
    }
})

// login
router.post('/', [
    check('email', 'Enter your email address').isEmail(),
    check('password', 'Enter your password').exists(),
], async (req, res) => {
    const {email, password} = req.body;
    try{
        let user = await User.findOne({email})
        if(!user){
            return res.status(404).json({msg: 'User not registered'});
        }
        const isMatch = await bcrypt.compare(password, user.password)
        if(!isMatch){
            return res.status(401).json({msg: 'Password is incorrect'});
        }

        const payload = {
            user: {
                id: user.id,
            }
        }

        jwt.sign(
            payload,
            config.get('jwtSecret'),
            {expiresIn: 360000},
            (err, token) => {
                if(err) throw err;
                res.json({token});
            }
        )


    }catch(err){
        console.error(err.message)
        res.status(500).send('Server error')
    }
})

module.exports = router