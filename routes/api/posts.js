const express = require('express')
const router = express.Router()
const {check, validationResult} = require('express-validator')
const auth = require('../../middleware/auth')
const Post = require('../../models/Post')
const Profile = require('../../models/Profile')
const User = require('../../models/User')

// create post
router.post('/', [
    auth,
    [
        check('text', 'Text is required').not().isEmpty()
    ]
], async(req, res) => {
    const errors = validationResult(req)
    if(!errors.isEmpty()){
        return res.status(400).json({errors: errors.array()})
    }

    try {
        const user = await User.findById(req.user.id).select('-password')
        const newPost = new Post({
            text: req.body.text,
            name: user.name,
            avatar: user.avatar,
            user: req.user.id
        })

        const post = await newPost.save()

        res.status(200).json(post)

    } catch (err) {
        console.error(err.message)
        res.status(500).send('Server error')
    }

})

// get all post
router.get('/', auth, async(req, res) => {
    try {
        const posts = await Post.find().sort({date: -1})
        res.json(posts)
    } catch (err) {
        console.error(err.message)
        res.status(500).send('Server error')
    }
})

// get post by id
router.get('/:post_id', auth, async(req, res) => {
    try {
        const post = await Post.findById(req.params.post_id)
        if(!post){
            return res.status(404).json({msg: 'Post not found'})
        }
        res.json(post)
    } catch (err) {
        console.error(err.message)
        if(err.kind === 'ObjectId'){
            return res.status(404).json({msg: 'Post not found'})
        }
        res.status(500).send('Server error')
    }
})

// delete post by _id
router.delete('/:post_id', auth, async(req, res) => {
    try {
        const post = await Post.findById(req.params.post_id)

        if(!post){
            return res.status(404).json({msg: 'Post not found'})
        }

        if(post.user.toString() !== req.user.id){
            return res.status(401).json({msg: 'User not authorized'})
        }

        await post.remove()

        res.json({msg: 'Post removed'})
    } catch (err) {
        console.error(err.message)
        if(err.kind === 'ObjectId'){
            return res.status(404).json({msg: 'Post not found'})
        }
        res.status(500).send('Server error')
    }
})

// like
router.put('/like/:id', auth, async(req, res) => {
    try {
        const post = await Post.findById(req.params.id)

        // check if user has been liked
        if(post.likes.filter(like => like.user.toString() === req.user.id).length > 0){
            return res.status(400).json({msg: 'Post already liked'})
        }

        post.likes.unshift({user: req.user.id})
        await post.save()
        res.json(post)
    } catch (err) {
        console.error(err.message)
        res.status(500).send('Server error')
    }
})

// unlike
router.put('/unlike/:id', auth, async(req, res)=>{
    try {
        const post = await Post.findById(req.params.id)
        if(post.likes.filter(like => like.user.toString() === req.user.id).length === 0){
            return res.status(400).json({msg: 'Post has not liked yet'})
        }

        const removeIndex = post.likes.map(like => like.user.toString()).indexOf(req.user.id)

        post.likes.splice(removeIndex, 1)
        await post.save()
        res.json(post)
    } catch (err) {
        console.error(err.message)
        res.status(500).send('Server error')
    }
})

// add comments
router.put('/comment/:postId', [
    auth,
    [
        check('text', 'Text is required').not().isEmpty(),
    ]
], async(req, res) => {
    const errors = validationResult(req)
    if(!errors.isEmpty()){
        return res.status(400).json({errors: errors.array()})
    }
    try {
        const post = await Post.findById(req.params.postId)
        const user = await User.findById(req.user.id).select('-password')
        let newComment = {
            text: req.body.text,
            user: req.user.id,
            name: user.name,
            avatar: user.avatar
        }

        post.comments.unshift(newComment)
        await post.save()
        res.json(post)
    } catch (err) {
        console.error(err.message)
        res.status(500).status('Server error')
    }
})

// remove comments
router.delete('/comment/:postId/:commentId', auth, async(req, res)=>{
    try {
        const post = await Post.findById(req.params.postId);
        const comment = post.comments.find(comment => comment.id == req.params.commentId);
        // check comment
        if(!comment){
            return res.status(404).json({msg: 'Comment not found'})
        }
        // check users
        if(comment.user.toString() !== req.user.id){
            return res.status(401).json({msg: 'User not authorized'});
        }

        const removeIndex = post.comments.map(comment => comment.user.toString()).indexOf(req.user.id)

        post.comments.splice(removeIndex, 1)

        await post.save()
        res.json(post)
    } catch (err) {
        console.error(err.message)
        res.status(500).send('Server error')
    }
})

module.exports = router