const express = require('express')
const router = express.Router()
const auth = require('../../middleware/auth')
const request = require('request')
const config = require('config')
const Profile = require('../../models/Profile')
const User = require('../../models/User')
const {check, validationResult} = require('express-validator')

// get profile user
router.get('/me', auth, async (req, res) => {
    try{
        const profile = await Profile.findOne({user: req.user.id})
        if(!profile){
            return res.status(404).json({msg:'Profile for this user is not found'});
        }
        res.json(profile);
    }catch(err){
        console.error(err.message)
        res.status(500).send(`Server error : ${err.message}`)
    }
})

// create profile & update profile
router.post('/', [auth, [
    check('status', 'Status is required').not().isEmpty(),
    check('skills', 'Status is required').not().isEmpty(),
]] ,async (req, res) => {
    const errors = validationResult(req)
    if(!errors.isEmpty()){
        return res.status(400).json({errors: errors.array()});
    }
    const {
        company,
        website,
        location,
        bio,
        status,
        githubusername,
        skills,
        youtube,
        facebook,
        twitter,
        instagram,
        linkedin
    } = req.body

    // build profile object
    const profileField = {}
    profileField.user = req.user.id
    if(company) profileField.company = company
    if(website) profileField.website = website
    if(location) profileField.location = location
    if(bio) profileField.bio = bio
    if(status) profileField.status = status
    if(githubusername) profileField.githubusername = githubusername
    if(skills){
        profileField.skills = skills.split(', ').map(skill => skill.trim())
    }

    // build object social profile
    profileField.social = {}
    if(youtube) profileField.social.youtube = youtube
    if(twitter) profileField.social.twitter = twitter
    if(facebook) profileField.social.facebook = facebook
    if(linkedin) profileField.social.linkedin = linkedin
    if(instagram) profileField.social.instagram = instagram

    try {
        let profile = await Profile.findOne({user: req.user.id})

        if(profile){
            // update
            profile = await Profile.findOneAndUpdate({user: req.user.id}, {$set: profileField}, {new:true})
            return res.json(profile)
        }

        // create
        profile = new Profile(profileField)
        await profile.save()
        res.json(profile)
    } catch (err) {
        console.error(err.message)
        res.status(500).send('Server error')
    }

})

// get all profile
router.get('/', async(req, res) => {
    try{
        const profiles = await Profile.find().populate('user', ['name', 'avatar'])
        res.json(profiles)
    }catch(err){
        console.error(err.message)
        res.status(500).send('Server error')
    }
})

// get profil by id
router.get('/user/:user_id', async(req, res) => {
    try{
        const profile = await Profile.findOne({user: req.params.user_id}).populate('user', ['name', 'avatar'])

    }catch(err){
        console.error(err.messsage)
        if(err.kind == 'ObjectId'){
            return res.status(400).json({msg: 'Profile not found'})
        }
        res.status(500).send('Server error')
    }
})

// delete profile and user
router.delete('/', auth, async (req, res) => {
    try{
        await Profile.findOneAndRemove({user: req.user.id})
        await User.findOneAndRemove({_id: req.user.id})
        res.json({msg: 'User deleted'})
    }catch(err){
        console.error(err.message)
        res.status(500).send('Server error')
    }
})

// add experience profile
router.put('/experience', [auth, [
    check('title', 'Title is required').not().isEmpty(),
    check('company', 'Company is required').not().isEmpty(),
    check('from', 'From data is required').not().isEmpty(),
]], async (req, res) => {
    const errors = validationResult(req)
    if(!errors.isEmpty()){
        return res.status(400).json({errors: errors.array()})
    }

    const {title,
        company,
        location,
        from,
        to,
        current,
        description} = req.body

    const newExp = {
        title,
        company,
        location,
        from,
        to,
        current,
        description
    }

    try {
        const profile = await Profile.findOne({user: req.user.id})
        profile.experience.unshift(newExp)
        await profile.save()
        res.json(profile)
    } catch (err) {
        console.error(err.message)
        res.status(500).send('Server error')
    }
})

// remove experience profile
router.delete('/experience/:exp_id', auth, async(req, res) => {
    try {
        const profile = await Profile.findOne({user: req.user.id})
        const removeIndex = profile.experience
        .map(item => item._id)
        .indexOf(req.params.id)
        profile.experience.splice(removeIndex, 1);
        await profile.save();
        res.json(profile)
    } catch (err) {
        console.error(err.message)
        res.status(500).send('Server error')
    }
})

router.put('/education', [
    auth,
    [
        check('school', 'School is required').not().isEmpty(),
        check('degree', 'Degree is required').not().isEmpty(),
        check('fieldofstudy', 'Fieldofstudy is required').not().isEmpty(),
        check('from', 'From data is required').not().isEmpty(),
    ]
], async(req, res) => {
    const errors = validationResult(req)
    if(!errors.isEmpty()){
        return res.status(400).json({errors: errors.array()})
    }
    const {
        school,
        degree,
        fieldofstudy,
        from,
        to,
        current,
        description
    } = req.body

    const newEdu = {
        school,
        degree,
        fieldofstudy,
        from,
        to,
        current,
        description
    }

    try{
        const profile = await Profile.findOne({user: req.user.id})
        profile.education.unshift(newEdu)
        await profile.save()
        res.json(profile)
    }catch(err){
        console.error(err.message)
        res.status(500).send('Server error')
    }

})

// delete education
router.delete('/education/:edu_id', auth, async(req, res) => {
    try{
        const profile = await Profile.findOne({user: req.user.id})
        const removeIndex = profile.education
        .map(item => item.id)
        .indexOf(req.params.edu_id)

        profile.education.splice(removeIndex, 1)
        await profile.save()
        res.json(profiles)
    }catch(err){
        console.error(err.message)
        res.status(500).send('Server error')
    }
})

// github profile
router.get('/github/:username', (req, res) => {
    try {
        const options = {
            uri: `https://api.github.com/users/${req.params.username}/repos?per_page=5&sort=created:asc&client_id=${config.get('githubClientId')}&client_secret=${config.get('githubClientSecret')}`,
            method: 'GET',
            headers: {'user-agent':'node.js'}
        }

        request(options, (error, response, body) => {
            if(error) console.error(error);
            if(response.statusCode !== 200){
                res.status(404).json({msg: 'No github profile found'})
            }

            res.json(JSON.parse(body))
        })
    } catch (err) {
        console.error(err.message)
        res.status(500).send('Server error')
    }
})

module.exports = router