import React, {Fragment, useEffect} from 'react'
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import './App.css';
// components
import Landing from './components/layout/Landing'
import Navbar from './components/layout/Navbar'
import Login from './components/auth/Login'
import Register from './components/auth/Register'
import Alert from './components/layout/Alert'
import Dashboard from './components/dashboard/Dashboard'
import PrivateRoute from './components/routing/PrivateRoute'
// redux
import { Provider } from 'react-redux';
import store from './store';
// actions
import {loadUser} from './actions/auth'
// utils
import setAuthToken from './utils/setAuthToken'

if(localStorage.token){
  setAuthToken(localStorage.token)
}

const App = () => {

  useEffect(() => {
    store.dispatch(loadUser())
  }, [])

  return(
    <Provider store={store} >
      <Router>
        <Fragment>
          <Navbar/>
          <Routes>
            <Route exact path="/" element={<Landing/>} />
          </Routes>
          <section className="container">
            <Alert />
            <Routes>
              <Route  path="register" element={<Register/>} />
              <Route  path="login" element={<Login/>} />
              <Route
                path="/dashboard"
                element={<PrivateRoute component={Dashboard} />}
              />
            </Routes>
          </section>
        </Fragment>
      </Router>
    </Provider>
  )
}

export default App;
